﻿using UnityEngine;
using System.Collections;

namespace Completed
{
    public class Loader : MonoBehaviour
    {
        public GameObject gameManager;          //GameManager prefab to instantiate.

        void Awake()
        {
            //Check if a GameManager has already been assigned to static variable Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.Completed.GameManager.instance or if it's still null
            if (Completed.GameManager.instance == null)

                //Instantiate gameManager prefab
                Instantiate(gameManager);
        }
    }
}