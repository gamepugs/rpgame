﻿using System;

using UnityEngine;
using UnityEngine.UI;

using CaracterControlls;

public class CharacterStatus : MonoBehaviour
{
    bool isDead, damaged;
    public int startingHealth = 500;
    public int startingShield = 300;
    public float currentHealth, currentShield;
    public float flashSpeed = 5f;

    public Slider shieldSlider, healthSlider;
    public Image damageImage;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

    Animator anim;
    Character character;

    void Awake()
    {
        currentHealth = startingHealth;
        currentShield = startingShield;

        anim = GetComponent<Animator>();

        character = GetComponent<Character>();
        character.StatusChanged += CharacterHealthChanged;
    }

    void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        damaged = false;
    }

    public void TakeDamage(float health, float shield)
    {
        damaged = true;

        currentHealth = health;
        currentShield = shield;

        healthSlider.value = currentHealth;
        shieldSlider.value = currentShield;

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    void Death()
    {
        isDead = true;

        //anim.SetTrigger("Die");

        character.enabled = false;
    }

    void CharacterHealthChanged(object sender, CharacterStatusEventArgs e)
    {
        if (Math.Abs(currentHealth - e.Health) > float.Epsilon || Math.Abs(currentShield - e.Shield) > float.Epsilon)
        {
            TakeDamage(e.Health, e.Shield);
        }
    }
}