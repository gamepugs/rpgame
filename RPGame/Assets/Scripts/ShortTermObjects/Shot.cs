﻿using System;
using CaracterControlls;
using Managers;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ShortTermObjects
{
	public class Shot {

		private Character owner;

		private Boolean isAlive;
		public Boolean IsAlive
		{
			get { return isAlive; }
		}
	
		public GameObject shot;

		public Shot(Character _owner)
		{
			owner = _owner;

			GameObject prefab = Resources.Load("Shot") as GameObject;
			shot = Object.Instantiate(prefab);
			
			shot.AddComponent<BulletMovement>();
			shot.transform.parent = GameObject.Find("Ammo").transform;

		
			shot.SetActive(false);
			isAlive = false;
		}

		public void Revive()
		{
			shot.SetActive(true);
			isAlive = true;
			shot.transform.rotation = owner.transform.rotation;
			shot.transform.position = owner.transform.position;

			Vector3 direction = shot.transform.up;
			
			shot.GetComponent<BulletMovement>().Go(owner.transform.position, direction, this);
		}
	
		public void Die()
		{
			shot.SetActive(false);
			isAlive = false;
			PoolManager.instance.KillShot(owner);
		}

		public Character GetOwner()
		{
			return owner;
		}
	}
}
