using System.Collections.Generic;
using CaracterControlls;
using UnityEngine;

namespace ShortTermObjects
{
	public class ShotPool {

		List<Shot> pool = new List<Shot>();
		private int index = 0;
	
		public ShotPool(Character owner, int count)
		{
			GameObject ammo = new GameObject("Ammo");
			for (int i = 0; i < count; i++)
			{
				pool.Add(new Shot(owner));
			}
		}
	
		public Shot GetPooledObject()
		{
			if (index != pool.Count)
			{
				Shot obj = pool[index];
				obj.Revive();
				index++;
				return obj;
			}
			return null;
		}

		public void KillPooledObject()
		{
			if (index != 0)
			{
				index--;
			}
			pool.Sort((x,y) => x.IsAlive.CompareTo(y.IsAlive) * -1);
		}
	}
}
