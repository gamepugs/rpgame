﻿using System.Collections;
using Biosphere;
using Managers;
using UnityEngine;

namespace ShortTermObjects
{
	public class BulletMovement : MonoBehaviour {
		protected float stepSize = 20f;
		protected float stepTime = 2.2f;

		private Shot shot;

		// Starts corotile that movemes object
		public void Go(Vector3 owner_position, Vector3 direction, Shot shot)
		{
			this.shot = shot;
		
			StartCoroutine(LinearMove(owner_position, owner_position + direction * stepSize, stepTime));
		}

		protected IEnumerator LinearMove(Vector3 from, Vector3 to, float duration)
		{
			Vector3 prevPos = Vector3.forward;

			float agregate = 0;
			while (agregate < stepSize)
			{
				agregate += Time.deltaTime / duration;
				transform.position = Vector3.Lerp(from, to, agregate);

				if (prevPos == transform.position)
					break;

				prevPos = transform.position;
				yield return null;
			}
			shot.Die();
		}
		
		// On bullet hits something
		void OnTriggerEnter2D (Collider2D col)
		{
			// On hit Mob
			if (col.gameObject.GetComponent<AIMovement>() != null)
			{
				DamageRecorder mob = col.gameObject.GetComponent<DamageRecorder>();
				if (mob.IsAlive())
				{
					shot.Die();
					mob.DealDamage(shot.GetOwner().GetDamage());
				}
			}
		}
	
	}
}
