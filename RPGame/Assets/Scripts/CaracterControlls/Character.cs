﻿using System;

using UnityEngine;
using UnityEngine.SceneManagement;

using Biosphere;

using Random = UnityEngine.Random;

namespace CaracterControlls
{
    public class Character : MovingObject
    {
        private int expPoints;

        private float hitPoints;
        private float currHitPoints;

        private float shieldPoints;
        private float currShieldPoints;

        private float damage;

        private float shootingSpeed;

        public float restartLevelDelay = 1f;
        public int pointsPerFood = 10;
        public int pointsPerSoda = 20;
        public int wallDamage = 1;

        private Animator animator;

        public event EventHandler<CharacterStatusEventArgs> StatusChanged;

        public float GetShootingSpeed()
        {
            return shootingSpeed;
        }

        private Boolean isAlive;
        public Boolean IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        protected override void Start()
        {
            expPoints = 0;
            hitPoints = 500;
            shieldPoints = 300;
            shootingSpeed = 0.18F;
            damage = 100;
            isAlive = true;

            currHitPoints = hitPoints;
            currShieldPoints = shieldPoints;

            animator = GetComponent<Animator>();

            base.Start();
        }

        private void Update()
        {
            int horizontal = 0;     //Used to store the horizontal move direction.
            int vertical = 0;       //Used to store the vertical move direction.

            //Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
            horizontal = (int)(Input.GetAxisRaw("Horizontal"));

            //Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
            vertical = (int)(Input.GetAxisRaw("Vertical"));

            //Check if moving horizontally, if so set vertical to zero.
            if (horizontal != 0)
            {
                vertical = 0;
            }

            //Check if we have a non-zero value for horizontal or vertical
            if (horizontal != 0 || vertical != 0)
            {
                //Call AttemptMove passing in the generic parameter Wall, since that is what Player may interact with if they encounter one (by attacking it)
                //Pass in horizontal and vertical as parameters to specify the direction to move Player in.
                AttemptMove<Wall>(horizontal, vertical);
            }

            if (currHitPoints <= 0)
            {
                isAlive = false;
                SpriteRenderer charSprite = GetComponent<SpriteRenderer>();
                charSprite.sprite = Resources.Load<Sprite>("Blood");
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.5F); // Drawing remains lower then Mobs

                // Removing physics from blood spots;
                BoxCollider2D playerBoxCollider2D = gameObject.GetComponent<BoxCollider2D>();
                if(playerBoxCollider2D != null)
                    playerBoxCollider2D.isTrigger = true;
                
                currHitPoints = 0;

                var handler = StatusChanged;

                if (handler != null)
                {
                    handler(this, new CharacterStatusEventArgs(currHitPoints, currShieldPoints));
                }

                GameManager.instance.GameOver();
            }
        }

        public float GetDamage()
        {
            float koef = Random.Range(0.75F, 1F);
            return damage * koef;
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Exit")
            {
                Invoke("Restart", restartLevelDelay);

                enabled = false;
            }
            else if (other.tag == "Food")
            {
                other.gameObject.SetActive(false);
            }
            else if (other.tag == "Soda")
            {
                other.gameObject.SetActive(false);
            }
            else
            {
                TakeDamage(other);
            }
        }

        void OnTriggerStay2D(Collider2D col)
        {
            TakeDamage(col);
        }

        void TakeDamage(Collider2D col)
        {
            if (col.gameObject.GetComponent<DamageRecorder>() != null)
            {
                DamageRecorder mob = col.gameObject.GetComponent<DamageRecorder>();

                if (mob.IsAlive() && isAlive && mob.GetAttackDelay() < 0) // Mob & Character are alive
                {
                    if (currShieldPoints > 0)
                    {
                        currShieldPoints -= mob.GetMob().GetBreed().GetDamage();
                        currShieldPoints = Math.Max(currShieldPoints, 0);
                    }
                    else
                    {
                        currHitPoints -= mob.GetMob().GetBreed().GetDamage();
                        currHitPoints = Math.Max(currHitPoints, 0);
                    }

                    var handler = StatusChanged;
                    if (handler != null)
                    {
                        handler(this, new CharacterStatusEventArgs(currHitPoints, currShieldPoints));
                    }


                    if (currHitPoints <= 0)
                    {
                        isAlive = false;
                        SpriteRenderer charSprite = GetComponent<SpriteRenderer>();
                        charSprite.sprite = Resources.Load<Sprite>("Blood");
                        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.5F); // Drawing remains lower then Mobs
                        
                        // Removing physics from blood spots;
                        BoxCollider2D playerBoxCollider2D = gameObject.GetComponent<BoxCollider2D>();
                        if(playerBoxCollider2D != null)
                            playerBoxCollider2D.isTrigger = true;
                        
                    }

                    mob.SetAttackDelay();
                }
            }
        }


        //This function is called when the behaviour becomes disabled or inactive.
        void OnDisable()
        {
            //When Player object is disabled, store the current local food total in the GameManager so it can be re-loaded in next level.
            if(GameManager.instance != null)
                GameManager.instance.playerFoodPoints = 100;
        }

        //AttemptMove overrides the AttemptMove function in the base class MovingObject
        //AttemptMove takes a generic parameter T which for Player will be of the type Wall, it also takes integers for x and y direction to move in.
        protected override void AttemptMove<T>(int xDir, int yDir)
        {
            //Call the AttemptMove method of the base class, passing in the component T (in this case Wall) and x and y direction to move.
            base.AttemptMove<T>(xDir, yDir);

            //Hit allows us to reference the result of the Linecast done in Move.
            RaycastHit2D hit;

            //If Move returns true, meaning Player was able to move into an empty space.
            if (Move(xDir, yDir, out hit))
            {
            }
        }

        //OnCantMove overrides the abstract function OnCantMove in MovingObject.
        //It takes a generic parameter T which in the case of Player is a Wall which the player can attack and destroy.
        protected override void OnCantMove<T>(T component)
        {
            //Set hitWall to equal the component passed in as a parameter.
            Wall hitWall = component as Wall;

            //Call the DamageWall function of the Wall we are hitting.
            hitWall.DamageWall(wallDamage);

            //Set the attack trigger of the player's animation controller in order to play the player's attack animation.
            animator.SetTrigger("playerChop");
        }


        //Restart reloads the scene when called.
        void Restart()
        {
            //Load the last scene loaded, in this case Main, the only scene in the game. And we load it in "Single" mode so it replace the existing one
            //and not load all the scene object in the current scene.
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
        }
    }
}