﻿using System.Collections;
using UnityEngine;

namespace CaracterControlls
{
    public class Movement : MonoBehaviour
    {

        [SerializeField]
        protected float stepSize = 0.5f;
        [SerializeField]
        protected float stepTime = 0.15f;

        protected bool isMoving;

        public void Walk(Vector3 direction)
        {
            if (!isMoving)
            {
                StartCoroutine(LinearMove(transform.position, transform.position + direction * stepSize, stepTime));
            }
        }

        public void SetPosition(Vector3 pos)
        {
            transform.position = pos;
        }

        protected IEnumerator LinearMove(Vector3 from, Vector3 to, float duration)
        {
            isMoving = true;
            float agregate = 0;
            while (agregate < stepSize)
            {
                agregate += Time.deltaTime / duration;
                transform.position = Vector3.Lerp(from, to, agregate);

                yield return null;
            }
            isMoving = false;
        }
    }
}
