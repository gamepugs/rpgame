﻿using Managers;
using UnityEngine;

namespace CaracterControlls
{
    public class ShootCommand : Command 
    {
        public override void Execute(MonoBehaviour reciver)
        {
            PoolManager pm = PoolManager.instance;

            pm.InitiateShot(reciver.GetComponent<Character>());
        }
    }
}
