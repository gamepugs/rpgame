﻿using UnityEngine;

namespace CaracterControlls
{
    public class MoveCommand : Command
    {
        private Vector3 direction;
	
        public MoveCommand(Vector3 dir)
        {
            direction = dir;
        }
	
        public override void Execute(MonoBehaviour reciver)
        {
            Movement actor = reciver as Movement;
            if (actor != null)
            {
                actor.Walk(direction);
            }
        }

    }
}

