﻿using UnityEngine;

namespace CaracterControlls
{
    public class Rotation : MonoBehaviour {

        [SerializeField]
        public float rotateSpeed = 1.0F;
 
        public void  Update()
        {
            Camera camera = Camera.main;
		
            Vector3 mousePosition = camera.ScreenToViewportPoint(
                new Vector3(
                    Input.mousePosition.x, 
                    Input.mousePosition.y, 
                    Input.mousePosition.z - camera.transform.position.z
                ));
		
            mousePosition = new Vector3(
                mousePosition.x - camera.rect.xMax / 2,
                mousePosition.y - camera.rect.yMax / 2, 
                mousePosition.z
            );
		
            float z = Mathf.Atan2(mousePosition.y, mousePosition.x) * Mathf.Rad2Deg - 91;
            if(FindObjectOfType<Character>().IsAlive)
                transform.rotation = Quaternion.Euler(0, 0, z * rotateSpeed);
        }

    }
}
