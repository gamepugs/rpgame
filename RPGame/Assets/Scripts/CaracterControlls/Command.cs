﻿using UnityEngine;

namespace CaracterControlls
{
    public abstract class Command
    {
        public abstract void Execute(MonoBehaviour receiver);
    }
}
