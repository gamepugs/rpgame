﻿using CaracterControlls;
using Managers;
using ShortTermObjects;
using UnityEngine;

namespace Biosphere
{
	public class DamageRecorder : MonoBehaviour 
	{
		private Mob mob;

		private float deathDelay = 5F; // Time after which mob remeins disappear

		private float attackDelay;
	
		private void Update()
		{
			if(!mob.IsAlive)
				deathDelay -= Time.deltaTime;

			if (deathDelay < 0)
			{
				mob.Die();
				PoolManager.instance.KillMob("Zombie");
			}
		}

		public void DealDamage(float damage)
		{
			mob.DealDamage(damage);
		}

		public void SetMonsterInfo(Mob monster)
		{
			mob = monster;
			attackDelay = mob.GetBreed().GetAttackSpeed();
		}
	
		// On mob bumped into smth
		void OnTriggerEnter2D (Collider2D col)
		{
			// On hit by buller
			if (col.gameObject.GetComponent<BulletMovement>() != null)
			{
				if (mob.GetBreed().GetCurrHealth() < 0)
				{
					mob.IsAlive = false;
				}
			}
		
		}

		void OnTriggerStay2D (Collider2D col)
		{
			// Eating...
			if (col.gameObject.GetComponent<Character>() != null)
			{
				attackDelay -= Time.deltaTime;
			}
		}

		public bool IsAlive()
		{
			return mob.IsAlive;
		}

		public Mob GetMob()
		{
			return mob;
		}

		public float GetAttackDelay()
		{
			return  attackDelay;
		}

		public void SetAttackDelay()
		{
			attackDelay = mob.GetBreed().GetAttackSpeed();
		}
	}
}
