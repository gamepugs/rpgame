﻿using System.Collections;
using CaracterControlls;
using UnityEngine;

namespace Biosphere
{
	public class AIMovement : MonoBehaviour
	{
		private Mob mob;

		private bool stopMoving;
		public bool StopMoving
		{
			get { return stopMoving; }
			set { stopMoving = value; }
		}

		public void SetMonsterInfo(Mob monster)
		{
			mob = monster;
		}

		private void Start()
		{
			stopMoving = true;
		}

		void Update ()
		{
			Character character = FindObjectOfType<Character>();
			
			if (mob != null && mob.IsAlive && !stopMoving)
			{
				float z = Mathf.Atan2(character.transform.position.y - transform.position.y, character.transform.position.x - transform.position.x) * Mathf.Rad2Deg - 91;
				transform.rotation = Quaternion.Euler(0, 0, z);
				
				Vector3 direction = transform.up;
				Vector3 to = transform.position + direction * mob.GetBreed().GetStep();
				StartCoroutine(LinearMove(transform.position, to, mob.GetBreed().StepTime(), mob.GetBreed().GetStep()));
			}
		}

		protected IEnumerator LinearMove(Vector3 from, Vector3 to, float duration, float stepSize)
		{
			float agregate = 0;
			while (agregate < stepSize)
			{
				agregate += Time.deltaTime / duration;
				transform.position = Vector3.Lerp(from, to, agregate);
			
				yield return null;
			}
		}

		void OnTriggerEnter2D(Collider2D col)
		{
			// On bumped into character
			if (col.gameObject.GetComponent<Character>() != null)
			{
				stopMoving = true;
			}
		}
		
		void OnTriggerExit2D(Collider2D col)
		{
			// On bumped into character
			if (col.gameObject.GetComponent<Character>() != null)
			{
				stopMoving = false;
			}
		}

	}
}
