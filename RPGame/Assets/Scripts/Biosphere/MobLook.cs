﻿using System.Collections;
using System.Collections.Generic;
using Biosphere;
using CaracterControlls;
using UnityEngine;

public class MobLook : MonoBehaviour {
		
	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.GetComponent<Character>() != null)
		{
			transform.parent.GetComponent<AIMovement>().StopMoving = false;
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.GetComponent<Character>() != null)
		{
			transform.parent.GetComponent<AIMovement>().StopMoving = true;
		}
	}
}
