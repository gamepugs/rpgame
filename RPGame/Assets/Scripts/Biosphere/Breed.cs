﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Biosphere
{
    public class Breed 
    {
        private float _health;
        private float _currHealth;
        
        private float _damage;
        
        private float _stepSize;
        private float _stepTime;

        private float _attackSpeed;
	
        private GameObject _prefab;

        public Breed(float health, float stepSize, float stepTime, GameObject prefab)
        {
            _health = health;
            _stepSize = stepSize;
            _stepTime = stepTime;
            _prefab = prefab;
            _damage = 50;
            _attackSpeed = 0.8F;
        }

        public float GetAttackSpeed()
        {
            return _attackSpeed;
        }
        
        public void SetAttackSpeed(float attackSpeed)
        {
            _attackSpeed = attackSpeed;
        }
        
        public GameObject GetPrefab()
        {
            return _prefab;
        }

        public float GetHealth()
        {
            return _health;
        }
        
        public float GetCurrHealth()
        {
            return _currHealth;
        }
    
        public float GetStep()
        {
            return _stepSize;
        }

        public float StepTime()
        {
            return _stepTime;
        }

        public void SubtractHealth(float damage)
        {
            _currHealth -= damage;
        }

        public float GetDamage()
        {
            float koef = Random.Range(0.75F, 1F);
            return _damage * koef;
        }
    }
}
