﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Biosphere
{
	public class MobPool {
	
		List<Mob> pool = new List<Mob>();
		private int index = 0;
	
		public MobPool(int count)
		{
			GameObject ammo = new GameObject("Mobs");

			Random rnd = new Random(888);
			
			for (int i = 0; i < count; i++)
			{
				pool.Add(new Mob(GetRandomZombie(rnd)));
			}
		}

		// Here we can add factory
		private Breed GetRandomZombie(Random rnd)
		{
			if(rnd.Next(1,10) > 5)
				return new Breed(300, 0.5F, 0.35F, Resources.Load("FatZombie") as GameObject);
		
			return new Breed(150, 0.25F, 0.15F, Resources.Load("Zombie") as GameObject);
		}

		public Mob GetPooledObject(Vector3 position)
		{
			if (index != pool.Count)
			{
				Mob obj = pool[index];
				obj.Revive(position);
				index++;
				return obj;
			}
			return null;
		}

		public void KillPooledObject()
		{
			if (index != 0)
			{
				index--;
			}
			pool.Sort((x,y) => x.IsAlive.CompareTo(y.IsAlive) * -1);
		}
	}
}
