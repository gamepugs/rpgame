﻿using System;
using Managers;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Biosphere
{
  public class Mob
  {
    private GameObject mob;
    private Breed mobinfo;
    
    private Boolean isAlive;
    public Boolean IsAlive
    {
      get { return isAlive; }
      set { isAlive = value; }
    }
 
    BoxCollider2D mobBoxCollider2D;
    
    public Mob(Breed breed)
    {
      mobinfo = breed;
      mob = Object.Instantiate(breed.GetPrefab());
      
      mob.transform.parent = GameObject.Find("Mobs").transform;
      
      mob.AddComponent<AIMovement>();
      mob.GetComponent<AIMovement>().SetMonsterInfo(this);
      
      mob.AddComponent<DamageRecorder>();
      mob.GetComponent<DamageRecorder>().SetMonsterInfo(this);

      mob.GetComponent<Rigidbody2D>().mass = 15;

      mobBoxCollider2D = mob.GetComponent<BoxCollider2D>();

      mobBoxCollider2D.isTrigger = true;
      mob.SetActive(false);
      isAlive = false;
    }
  
    public void Revive(Vector3 position)
    {
      if (mob != null)
      {
        mob.transform.position = position;
        isAlive = true;

        // Restoring HP
        mobinfo.SubtractHealth(mobinfo.GetHealth() * -1);

        mob.SetActive(true);
        mobBoxCollider2D.isTrigger = false;
      }
    }
  
    public void Die()
    {
      isAlive = false;
      mob.SetActive(false);
    }

    public Breed GetBreed()
    {
      return mobinfo;
    }

    public void DealDamage(float damage)
    {
      mobinfo.SubtractHealth(damage);
      if (mobinfo.GetCurrHealth() < 0)
      {
        // Setting mob into blood-spot state
        SetPrefab(Resources.Load<Sprite>("Dirt"));
        mob.GetComponent<AIMovement>().StopMoving = true;
        isAlive = false;

        // Removing physics from blood spots;
        if(mobBoxCollider2D != null)
          mobBoxCollider2D.isTrigger = true;
      }
    }

    public void SetPrefab(Sprite sprite)
    {
      SpriteRenderer mobSprite = mob.GetComponent<SpriteRenderer>();
      mobSprite.sprite = sprite;
      mob.transform.position = new Vector3(mob.transform.position.x, mob.transform.position.y, mob.transform.position.z + 1.5F);
    }
  }
}
