﻿using System;

public class CharacterStatusEventArgs : EventArgs
{
    public float Health { get; private set; }
    public float Shield { get; private set; }

    public CharacterStatusEventArgs(float health, float shield)
    {
        Health = health;
        Shield = shield;
    }
}