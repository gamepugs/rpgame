﻿using UnityEngine;
using CaracterControlls;

public class TileEvent : MonoBehaviour
{
    public MapTile tile;

    public void OnTriggerEnter(Collider other)
    {
        tile.Enter(other.GetComponentInParent<Movement>());
    }
}