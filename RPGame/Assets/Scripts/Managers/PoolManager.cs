﻿using System;
using System.Collections.Generic;
using System.Linq;
using Biosphere;
using CaracterControlls;
using ShortTermObjects;
using UnityEngine;

namespace Managers
{
	public class PoolManager : MonoBehaviour {

		public static PoolManager instance = new PoolManager();
		Dictionary<Component, ShotPool> shootingPools = new Dictionary<Component, ShotPool>();
		Dictionary<String, MobPool> mobPools = new Dictionary<String, MobPool>();
		// <-- Add other pools here

		void Awake () {
			// Singletone stuff
			if (instance == null)
			{
				List<PoolManager> list = FindObjectsOfType<PoolManager>().ToList();
				instance = list[0];
			}
			else
			{
				Debug.Log("!!! Too much pool managers. Destroying...");
				GameObject currObj = GameObject.Find(name);
				if (currObj)
					Destroy(currObj, .5f);
			}
			DontDestroyOnLoad(instance);
			// Ends here
		
			foreach (Character player in FindObjectsOfType<Character>().ToList())
			{
				shootingPools.Add(player, new ShotPool(player, 100));
			}
		
			mobPools.Add("Zombie", new MobPool(50));
		}
	

		public Shot InitiateShot(Character character)
		{
			return shootingPools[character].GetPooledObject();
		}
	
		public void KillShot(Character character)
		{
			shootingPools[character].KillPooledObject();
		}

		public Mob SpawnMob(String mob, Vector3 position)
		{
			return mobPools[mob].GetPooledObject(position);
		}
		
		public void KillMob(String key) 
		{
			mobPools[key].KillPooledObject();
		}
	}
}
