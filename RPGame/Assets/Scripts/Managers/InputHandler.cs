﻿using System;
using System.Collections.Generic;
using CaracterControlls;
using UnityEngine;

namespace Managers
{
	public class InputHandler : MonoBehaviour {

		private Dictionary<String, Command> commands =  new Dictionary<String, Command>();
		private Movement player;

		private float shotDelay = 0; // Delay between shots
	
		// Use this for initialization
		void Start ()
		{
			player = FindObjectOfType<Movement>();
		
			commands.Add("UP", new MoveCommand(Vector3.up));
			commands.Add("UP_RIGHT", new MoveCommand((Vector3.up + Vector3.right) * 0.8F));
			commands.Add("UP_LEFT", new MoveCommand((Vector3.up + Vector3.left) * 0.8F));
		
			commands.Add("DOWN", new MoveCommand(Vector3.down));
			commands.Add("DOWN_RIGHT", new MoveCommand((Vector3.down + Vector3.right) * 0.8F));
			commands.Add("DOWN_LEFT", new MoveCommand((Vector3.down + Vector3.left) * 0.8F));

		
			commands.Add("RIGHT", new MoveCommand(Vector3.right));
			commands.Add("LEFT", new MoveCommand(Vector3.left));
		
			commands.Add("LMB", new ShootCommand());
		}
	
		// Update is called once per frame
		void Update () {

			if (player.gameObject.GetComponent<Character>().IsAlive)
			{
				shotDelay -= Time.deltaTime;

				// 0 - Left click
				if (Input.GetMouseButton(0))
				{
					if (shotDelay < 0)
					{
						commands["LMB"].Execute(player);
						shotDelay = player.GetComponent<Character>().GetShootingSpeed();
					}
				}


				if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
					commands["UP_RIGHT"].Execute(player);

				if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
					commands["UP_LEFT"].Execute(player);

				if (Input.GetKey(KeyCode.W))
					commands["UP"].Execute(player);

				if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
					commands["DOWN_RIGHT"].Execute(player);

				if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
					commands["DOWN_LEFT"].Execute(player);

				if (Input.GetKey(KeyCode.S))
					commands["DOWN"].Execute(player);

				if (Input.GetKey(KeyCode.D))
					commands["RIGHT"].Execute(player);

				if (Input.GetKey(KeyCode.A))
					commands["LEFT"].Execute(player);
			}

		}
	}
}
