﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Biosphere;
using Managers;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

	public static SpawnManager instance = new SpawnManager();

	private float spawnDelay;
	
	// Use this for initialization
	void Start () {
		// Singletone stuff
		if (instance == null)
		{
			List<SpawnManager> list = FindObjectsOfType<SpawnManager>().ToList();
			instance = list[0];
		}
		else
		{
			Debug.Log("!!! Too much pool managers. Destroying...");
			GameObject currObj = GameObject.Find(name);
			if (currObj)
				Destroy(currObj, .5f);
		}
		DontDestroyOnLoad(instance);
		// Ends here

		spawnDelay = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		spawnDelay -= Time.deltaTime;
		
		List<AIMovement> mobList = FindObjectsOfType<AIMovement>().ToList(); // List of mobs

		if (mobList.Count < 50 && spawnDelay < 0)
		{
			PoolManager.instance.SpawnMob("Zombie", new Vector3(10, 1, 1F));
			spawnDelay = 10;
		}
	}
}
